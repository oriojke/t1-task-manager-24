package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.model.User;

public interface IAuthService {

    @Nullable
    User registry(@NotNull String login, @NotNull String password, @NotNull String email);

    void login(@Nullable String login, @Nullable String password);

    void logout();

    boolean isAuth();

    @Nullable
    String getUserID();

    @NotNull
    User getUser();

    void checkRoles(@Nullable Role[] roles);

}
