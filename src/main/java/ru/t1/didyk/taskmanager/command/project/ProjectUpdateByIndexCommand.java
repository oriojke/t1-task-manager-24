package ru.t1.didyk.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Update project by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-update-by-index";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[UPDATE PROJECT BY INDEX");
        System.out.println("ENTER INDEX");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        getProjectService().updateByIndex(userId, index, name, description);
    }

}
