package ru.t1.didyk.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public class UserRemoveCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Remove user.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
