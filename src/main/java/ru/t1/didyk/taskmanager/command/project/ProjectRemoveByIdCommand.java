package ru.t1.didyk.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.model.Project;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Project project = getProjectService().findOneById(userId, id);
        getProjectTaskService().removeProjectById(userId, id);
    }

}
