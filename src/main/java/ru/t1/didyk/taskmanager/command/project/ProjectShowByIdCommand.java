package ru.t1.didyk.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.model.Project;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Show project by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Project project = getProjectService().findOneById(userId, id);
        showProject(project);
    }

}
