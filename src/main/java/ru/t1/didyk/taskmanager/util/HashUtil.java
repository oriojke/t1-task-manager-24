package ru.t1.didyk.taskmanager.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface HashUtil {

    String SECRET = "34534";

    Integer ITERATION = 7657;

    @Nullable
    static String salt(@Nullable final String value) {
        if (value == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < ITERATION; i++)
            result = md5(SECRET + result + SECRET);
        return result;
    }

    @Nullable
    static String md5(@Nullable final String value) {
        if (value == null) return null;
        try {
            @NotNull java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
